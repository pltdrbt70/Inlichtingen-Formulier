let firstName = new StorageItem('firstname', 'Niet ingevuld');
let lastName = new StorageItem('lastname', 'Niet ingevuld');
let dateOfBirth = new StorageItem('dateOfBirth', '00-00-0000');

let educationLevel = new StorageItem('educationLevel', 'Niet ingevuld');
let educationRoute = new StorageItem('educationRoute', 'Niet ingevuld');

function generateVmboPDF(){
    var doc = new jsPDF();
    doc.text("Voornaam: " + firstName.getValue(), 10, 20);
    doc.text("Achternaam: " + lastName.getValue(), 10, 30);
    doc.text("Geboortedatum: " + dateOfBirth.getValue(), 10, 40);
    doc.text("Achternaam: " + lastName.getValue(), 10, 50);
    doc.text("Niveau: " + educationLevel.getValue(), 10, 70);

    doc.save('Inlichtingen Formulier.pdf');
}
