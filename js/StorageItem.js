"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var StorageItem = function () {
    function StorageItem() {
        var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

        _classCallCheck(this, StorageItem);

        this.key = key;
        this.value = value;
    }

    _createClass(StorageItem, [{
        key: "getValue",
        value: function getValue() {
            this.value = atob(localStorage.getItem(window.btoa(this.key)));
            return this.value;
        }
    }, {
        key: "saveItem",
        value: function saveItem() {
            localStorage.setItem(window.btoa(this.key), window.btoa(this.value));
            return true;
        }
    }]);

    return StorageItem;
}();