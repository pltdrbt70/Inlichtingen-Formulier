let steplist_0 = document.getElementById('step0');

var app = angular.module("InlichtingenFormulier", ["ngRoute", "720kb.datepicker"]);
app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "views/welcome.html"
        })
        .when("/step1", {
            templateUrl : "views/step1.html"
        })
        .when("/step2", {
            templateUrl : "views/step2.html"
        })
        .when("/step3", {
            templateUrl : "views/step3.html"
        })
        .when("/step4", {
            templateUrl : "views/step4.html"
        })
        .when("/completed", {
            templateUrl : "views/stepCompleted.html"
        })
        .when("/printDocument", {
            templateUrl : "views/printDocument.html"
        });
});


