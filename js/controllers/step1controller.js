var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!

var yyyy = today.getFullYear();
if(dd<10){
    dd='0'+dd;
}
if(mm<10){
    mm='0'+mm;
}

var today = mm+'-'+dd+'-'+yyyy;

console.error(today);
app.controller('generalInfoController', function($scope) {
    $scope.curDate = today;
});

function nextStep(){
    var firstname = document.getElementById('firstname');
    var firstnameStorageObject = new StorageItem('firstname', firstname.value);
    firstnameStorageObject.saveItem();

    var lastname = document.getElementById('lastname');
    var lastnameStorageObject = new StorageItem('lastname', lastname.value);
    lastnameStorageObject.saveItem();

    var dob = document.getElementById('dateOfBirth');
    var dobStorageObject = new StorageItem('dateOfBirth', dob.value);
    dobStorageObject.saveItem();

    window.location.href = '#!/step2';
}