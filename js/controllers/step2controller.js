app.controller('educationInfoController', function($scope) {
    $scope.educationLevels = ['VMBO', 'HAVO', 'VWO', 'MBO', 'Anders'];
});

function nextStep3(){
    let educationLevel = document.getElementById('edu-level');
    let educationLevelStorageObject = new StorageItem('educationLevel', educationLevel.value);
    educationLevelStorageObject.saveItem();

    if (educationLevel.value === "VMBO"){
        //Handle VMBO

    } else if(educationLevel.value === "HAVO" || educationLevel.value === "VWO") {
        //Handle HAVO/VWO
        alert("HAVO/VWO");
    } else if(educationLevel.value === "MBO"){
        //Handle MBO
        alert("MBO");
    } else {
        //Handle otherwise
    }


    window.location.href = '#!/step3';
}